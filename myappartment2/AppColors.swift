//
//  AppColors.swift
//  WorkPilots
//
//  Created by developer on 12/16/15.
//  Copyright © 2015 developer. All rights reserved.
//

import Foundation
import UIKit

struct AppColors {
    
    static let Main1: Int = 0x33434B
    static let Main2: Int = 0x33434B
    
    static let BackGround: Int = 0xD1D7DA
    
    static let HexagonBorderColor: Int = 0xEBEBEB
    
    static let TextColor: Int = 0x000000
    
    static let MainColor: Int = Main1
    static let MainTextColor: Int = 0xffffff
}
