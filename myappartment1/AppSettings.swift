//
//  AppSettings.swift
//  myappartment1
//
//  Created by Shurygin Denis on 3/20/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class AppSettings: NSObject {

    public static let AppUrl = "https://beta-api.honkio.com/"
//    public static let AppUrl = "https://api.honkio.webdev.softdev.com/"
    public static let AppIdentity : Identity = Identity(id: "fi/honkio/myappartment",
                                                  andPassword: "gPin3jbiHlrehghmF99IDIiiDU0ZeZOEh9dGr6Rh")
    
//    public static let MEDIA_URL_PREFIX = "https://media1.honkio.com/bookahorse/getfile/"
    public static let MEDIA_URL_PREFIX = "https://media1.honkio.com/bookahorse_beta/getfile/"
    
    public static let MEDIA_URL_ASSET = MEDIA_URL_PREFIX + HK_OBJECT_TYPE_ASSET + "/"
    public static let MEDIA_URL_SHOP_PHOTO = MEDIA_URL_PREFIX + HK_OBJECT_TYPE_SHOP + "/"
}
