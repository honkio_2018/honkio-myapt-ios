//
//  GenericCollectionViewDataSource.swift
//  Honkio SDK
//
//  Created by developer on 1/4/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

/**
An object that adopts the UICollectionViewDataSource protocol is responsible
for providing the data and views required by a collection view.
Extends the UICollectionViewDataSource with the initialization action.
*/
open class GenericCollectionViewDataSource<TCollectionViewCell: UICollectionViewCell, TCollectionViewItem: AnyObject> : NSObject, UICollectionViewDelegate, UICollectionViewDataSource {
    /**
    Containes the collection of section.
    */
    open var sections: [HKCollectionSection<TCollectionViewItem>] = []
    
    var initializeCollectionViewAction:((TCollectionViewCell, TCollectionViewItem) -> Void)?
    
    var reusableIdentifier: String?

    /**
    Constructs the object.
    - parameter reusableIdentifier: The string identifier for reusing table cells.
    - parameter initializeCollectionViewAction: If set the action to be performed for initialization the cell with the item.
    */
    public init(reusableIdentifier: String?, initializeCollectionViewAction:@escaping (TCollectionViewCell, TCollectionViewItem) -> Void) {
        
        super.init()
        
        self.initializeCollectionViewAction = initializeCollectionViewAction
        self.reusableIdentifier = reusableIdentifier
    }

    /**
    - Returns: Number of sections.
    - parameter collectionView: The UICollectionView the function has been called for.
    */
    open func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return self.sections.count
    }

    /**
    Asks the data source object for the number of items in the specified section.
    - Returns: Number of items in section.
    - parameter collectionView: The UICollectionView the function has been called for.
    - parameter section: The index of section the number of items will be returned for.
    */
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.sections[section].items.count
    }

    /**
    Asks the data source object for the cell that corresponds to the specified item in the collection view.
    - Returns: The UICollectionViewCell for provided IndexPath.
    - parameter collectionView: The UICollectionView the function has been called for.
    - parameter indexPath: The IndexPath to be used for selecting the cell.
    */
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = self.sections[(indexPath as IndexPath).section].items[(indexPath as IndexPath).row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.reusableIdentifier ?? "", for: indexPath)
        self.initializeCollectionViewAction?(cell as! TCollectionViewCell, item)
        
        return cell
    }
}
