//
//  CollectionCellItem.swift
//  WorkPilots
//
//  Created by developer on 1/11/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit


enum CellType {
    
    case tabSauna
    case tabCharge
    case tabInvoice
    case tabPoll
    case tabServiceCall
    
    case undefined
}


class CollectionCellItem: NSObject {

    var backGroundColor: UIColor?
    
    var imageTintColor: UIColor?
    
    var primaryText: String?
    
    var primaryTintColor: UIColor?
    
    var imageName: String!
    
    var cellType = CellType.undefined
    
    var cellScale: Float = 1.0
    
    init(initializer: ((CollectionCellItem) -> Void)? = nil) {
        
        super.init()
        
        initializer?(self)
    }
}
