//
//  AppDelegate.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/14/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: HKBaseAppDelegate  {
    
    override func applicationShouldInit() {
        HonkioApi.initInstance(withUrl: AppSettings.AppUrl, app:AppSettings.AppIdentity)
        AppController.initInstance(AppViewControllerFactory(
            storyboards: ["Main"]))
        AppErrors.initInstance(BaseAppErrors())
        AppApi.initInstance(instance: AppApiInstance())
    }
    
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        
        UITabBar.appearance().tintColor = UIColor(netHex: AppColors.MainColor)
        UISwitch.appearance().onTintColor = UIColor(netHex: AppColors.MainColor)
        
        setupAppearanceForSettingsCell()
        setupAppearanceForAlertController()
        setupAppearanceForNavigationBar()
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func setupAppearanceForAlertController() {
        let view = UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self])
        view.tintColor = UIColor(netHex: AppColors.MainColor)
    }
    
    private func setupAppearanceForSettingsCell() {
        let view = UIView.appearance(whenContainedInInstancesOf: [HKSettingsTableViewCell.self])
        view.tintColor = UIColor(netHex: AppColors.MainColor)
    }
    
    private func setupAppearanceForWindow() {
        let view = UIView.appearance(whenContainedInInstancesOf: [UIWindow.self])
        view.tintColor = UIColor(netHex: AppColors.MainColor)
    }
    
    private func setupAppearanceForNavigationBar() {
        let navBarAppearance = UINavigationBar.appearance()
        navBarAppearance.barTintColor = UIColor(netHex: AppColors.MainColor)
        navBarAppearance.tintColor = UIColor(netHex: AppColors.MainTextColor)
        navBarAppearance.titleTextAttributes = [ NSAttributedString.Key.foregroundColor : UIColor(netHex: AppColors.MainTextColor) ]
    }

}

