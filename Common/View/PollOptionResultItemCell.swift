//
//  PollOptionResultItemCell.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/21/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit

class PollOptionResultItemCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgCheck.tintColor = UIColor(netHex: AppColors.MainColor)
    }
}
