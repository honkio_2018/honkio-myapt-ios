//
//  PollOptionItemCell.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/21/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit

class PollOptionItemCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.labelTitle.textColor = UIColor(netHex: AppColors.MainColor)
    }
}
