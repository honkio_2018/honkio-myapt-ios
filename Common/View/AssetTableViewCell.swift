//
//  AssetTableViewCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 23/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class AssetTableViewCell: UITableViewCell {

    @IBOutlet weak var assetImage: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    private let formatter = MeasurementFormatter()
    
    var asset: HKAsset? {
        didSet {
            self.titleLabel.text = self.asset?.name.uppercased()
            
            if let defaultPicture = self.asset?.properties?["default_picture"] {
                self.assetImage.imageFromURL(link: (defaultPicture as? String),
                                            errorImage: nil,
                                            contentMode: nil)
            } else {
                self.assetImage.image = nil
            }
            
            if let currentLocation = HonkioApi.deviceLocation() {
                let horseLocation = CLLocation(latitude: self.asset?.latitude ?? 0.0, longitude: self.asset?.longitude ?? 0.0)
                let distance = currentLocation.distance(from: horseLocation)
                let distanceInMeters = Measurement(value: distance, unit: UnitLength.meters)
                self.distanceLabel.text = self.formatter.string(from: distanceInMeters)
            }
            else {
                self.distanceLabel.text = ""
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let numberFormatter = NumberFormatter()
        numberFormatter.roundingMode = .ceiling
        numberFormatter .maximumFractionDigits = 1
        formatter.numberFormatter = numberFormatter
        formatter.unitOptions = .naturalScale
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
