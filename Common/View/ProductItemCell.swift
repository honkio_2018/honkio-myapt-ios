//
//  ProductItemCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/6/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class ProductItemCell: UITableViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
