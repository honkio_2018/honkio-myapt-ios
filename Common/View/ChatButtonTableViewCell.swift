//
//  ChatButtonTableViewCell.swift
//  myappartment1
//
//  Created by dev on 5/14/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class ChatButtonTableViewCell: UITableViewCell {
    
    var orderDetailsVC: Any?
//    OrderServiceCallDetailsViewController?
    
    @IBOutlet weak var chatButtonOutlet: UIButton!
    
    @IBAction func chatButtonAction(_ sender: UIButton) {
        
        if orderDetailsVC is OrderServiceCallDetailsViewController {
            let orderDetailsVC = self.orderDetailsVC as! OrderServiceCallDetailsViewController
            let controller = AppController.instance.getViewControllerByIdentifier("OrderChatViewController", viewController: orderDetailsVC) as! OrderChatViewController
            controller.chatOrder = orderDetailsVC.order
            orderDetailsVC.navigationController?.pushViewController(controller, animated: true)
        } else {
            let orderDetailsVC = self.orderDetailsVC as! BookOrderDetailsViewController
            let controller = AppController.instance.getViewControllerByIdentifier("OrderChatViewController", viewController: orderDetailsVC) as! OrderChatViewController
            controller.chatOrder = orderDetailsVC.order
            orderDetailsVC.navigationController?.pushViewController(controller, animated: true)
        }

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        chatButtonOutlet.layer.cornerRadius = 10
    }

}
