//
//  EventsSectionTitleCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/8/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class EventsSectionTitleCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
