//
//  MediaGalleryTableCell.swift
//  honkio-indx-ios
//
//  Created by Shurygin Denis on 12/6/18.
//  Copyright © 2018 RiskPointer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class MediaGalleryTableCell: UITableViewCell {
    
    public static let Identifier = "MediaGalleryTableCell"
    
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
}
