//
//  OrderHeaderViewCell.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/25/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OrderHeaderViewCell: UITableViewCell {
    
    @IBOutlet weak var assetImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var asset: HKAsset? {
        didSet {
            self.titleLabel.text = self.asset?.name.uppercased() ?? "..."
            
            if let defaultPicture = self.asset?.properties?["default_picture"] {
                self.assetImage.imageFromURL(link: (defaultPicture as? String),
                                             errorImage: nil,
                                             contentMode: nil)
            } else {
                self.assetImage.image = nil
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
}
