//
//  OrderTableViewCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 19/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class OrderServiceCallTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
}
