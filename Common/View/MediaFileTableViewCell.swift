//
//  SetImageCollectionViewCell.swift
//  honkio-safetypoint-ios
//
//  Created by Kartum on 12/01/19.
//  Copyright © 2019 Sunil Zalavadiya. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class MediaFileTableViewCell: HKMediaFileTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.mediaImage.clipsToBounds = true
        self.mediaImage.layer.cornerRadius = 20
    }

}
