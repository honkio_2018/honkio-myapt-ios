//
//  DetailedItemCell.swift
//  honkio-indx-ios
//
//  Created by Shurygin Denis on 12/6/18.
//  Copyright © 2018 RiskPointer. All rights reserved.
//

import UIKit

class DetailedItemCell: UITableViewCell {
    
    public static let Identifier = "DetailedItemCell"
    
    @IBOutlet weak var detailNameLabel: UILabel!
    @IBOutlet weak var detailInfoLabel: UILabel!
    
    private static let binder: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void) = { (viewController, cell, item) in
        let detailCell = cell as! DetailedItemCell
        let data = item as? (title: String, value: String?, tag: String?)
        
        detailCell.detailNameLabel.text = data?.title
        detailCell.detailInfoLabel.text = data?.value ?? "-"
    }
    
    public static func buildSourceItem(_ title: String, _ value: Double?) -> (identifierOrNibName: String, item: Any?, initializer: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)?) {
        return DetailedItemCell.buildSourceItem(title, value != nil ? String(value!) : nil)
    }
    
    public static func buildSourceItem(_ title: String, _ value: Int?) -> (identifierOrNibName: String, item: Any?, initializer: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)?) {
        return DetailedItemCell.buildSourceItem(title, value != nil ? String(value!) : nil)
    }
    
    public static func buildSourceItem(_ title: String, _ value: String?) -> (identifierOrNibName: String, item: Any?, initializer: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)?) {
        return DetailedItemCell.buildSourceItem(title, value, nil)
    }
    
    public static func buildSourceItem(_ title: String, _ value: String?, _ tag: String?) -> (identifierOrNibName: String, item: Any?, initializer: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)?) {
        // for empty string fields check empty (ex. bankSwift)
        return (DetailedItemCell.Identifier, (title: title, value: value != nil && value!.isEmpty ? nil : value, tag: tag), DetailedItemCell.binder)
    }
    
}
