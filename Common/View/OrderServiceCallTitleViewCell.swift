//
//  OrderHeaderViewCell.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/25/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OrderServiceCallTitleViewCell: UITableViewCell {
    
    public static let Identifier = "OrderServiceCallHeaderViewCell"
    
  
    @IBOutlet weak var titleLabel: UILabel!
    
    var order: Order? {
        didSet {
            self.titleLabel.text = self.order?.title.uppercased() ?? "..."
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
}
