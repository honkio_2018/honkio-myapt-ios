//
//  PollItemCell.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/21/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit

class PollItemCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelText: UILabel!
    
}
