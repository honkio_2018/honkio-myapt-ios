//
//  HexagonViewCell.swift
//  WorkPilots
//
//  Created by developer on 1/4/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

class HexagonViewCell: UICollectionViewCell {

    fileprivate let _defaultLineWidth: CGFloat = 4
    
    fileprivate let _defaultCornerRadius: CGFloat = 10
    
    var lineWidth: CGFloat? = 4
    
    var cornerRadius: CGFloat? = 10
    
    var fillColor: UIColor? = UIColor.red
    
    var borderColor: UIColor? = UIColor.white
    
    let layerMask = CAShapeLayer()
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var imageLeftMargin: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
    }
    
    
    func initialize() {
        
        let path = self.makeRoundedPolygonPath(self.bounds, lineWidth: lineWidth ?? _defaultLineWidth, sides: 6, cornerRadius: cornerRadius ?? _defaultCornerRadius)
        
        layerMask.path = path.cgPath
        layerMask.lineWidth = lineWidth ?? _defaultLineWidth
        layerMask.strokeColor = UIColor.clear.cgColor
        layerMask.fillColor = UIColor.white.cgColor
        self.layer.mask = layerMask
        
        let border = CAShapeLayer()
        
        border.path = path.cgPath
        border.lineWidth = lineWidth ?? _defaultLineWidth
        border.strokeColor = self.borderColor?.cgColor
        border.fillColor = self.fillColor?.cgColor
    
        self.layer.addSublayer(border)
    }
    
    fileprivate func makeRoundedPolygonPath(_ square: CGRect, lineWidth: CGFloat, sides: Int, cornerRadius: CGFloat) -> UIBezierPath {
        
        let path = UIBezierPath()
        let theta = Double(2) * Double.pi / Double(sides)
        let offset = cornerRadius * CGFloat(tanf(Float(theta * Double(0.5))))
        let squareWidth = min(square.size.width, square.size.height)
        
        var lenght = squareWidth - lineWidth
        
        if sides % 4 != 0 {
            
            lenght = lenght * CGFloat(cosf(Float(theta * Double(0.5)))) + offset * 0.5
        }
        
        let sideLenght = lenght * CGFloat(tanf(Float(theta * Double(0.5))))
        
        // start drawing at `point` in lower right corner
        var point = CGPoint(x: squareWidth * 0.5 + sideLenght * 0.5 - offset, y: squareWidth - (squareWidth - lenght) * 0.5)
        path.move(to: point)
        
        var angle = Float.pi
        
        // draw the sides and rounded corners of the polygon
        for _ in 0 ..< sides {
            
            point = CGPoint(x: point.x + (sideLenght - offset * 2.0) * CGFloat(cosf(angle)), y: point.y + (sideLenght - offset * 2.0) * CGFloat(sinf(angle)))
            path.addLine(to: point)
            
            let center = CGPoint(x: point.x + cornerRadius * CGFloat(cosf(angle + Float(Double.pi / 2))), y: point.y + cornerRadius * CGFloat(sinf(angle + Float(Double.pi / 2))))
            path.addArc(withCenter: center, radius: cornerRadius, startAngle: CGFloat(angle - Float(Double.pi / 2)), endAngle: CGFloat(angle + Float(theta) - Float(Double.pi / 2)), clockwise: true)
            
            point = path.currentPoint // we don't have to calculate where the arc ended ... UIBezierPath did that for us
            angle += Float(theta)
        }
        
        path.close()
        
        return path
    }
}
