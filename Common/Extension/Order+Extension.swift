//
//  Order+Extension.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/22/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

extension Order {
    
    static let Model = (
        BOOK: "smartcity",
        SERVICE_CALL: "asuntosi_service"
    )
    
    static let BookStatus = (
        BOOK_REQUEST: "book_request",
        BOOKED: "booked",
        CANCELED_BY_USER: "cancelled_by_user",
        CANCELED_BY_MERCHANT: "cancelled_by_merchant",
        COMPLETED: "completed"
    )
    
    static let ServiceCallStatus = (
        REQUEST: "new",
        IN_PROGRESS: "in_progress",
        CANCELED_BY_CUSTOMER: "cancelled_by_customer",
        COMPLETED: "completed"
    )
    
    private static let FieldComment = "comment"
    
    var comment: String? {
        set {
            self.setCustom(Order.FieldComment, value: newValue!)
        }
        get {
            return getCustom(Order.FieldComment) as? String
        }
    }
    
    
    var statusName: String {
        get {
            switch (self.status) {
            case Order.BookStatus.BOOK_REQUEST:
                return Str.order_status_book_request
            case Order.BookStatus.BOOKED:
                return Str.order_status_booked
            case Order.BookStatus.CANCELED_BY_USER:
                return Str.order_status_cancelled_by_owner
            case Order.BookStatus.CANCELED_BY_MERCHANT:
                return Str.order_status_cancelled_by_customer
            case Order.BookStatus.COMPLETED:
                return Str.order_status_completed
            case Order.ServiceCallStatus.REQUEST:
                return Str.order_service_call_status_new
            case Order.ServiceCallStatus.IN_PROGRESS:
                return Str.order_service_call_status_in_progress
            case Order.ServiceCallStatus.CANCELED_BY_CUSTOMER:
                return Str.order_service_call_status_cancelled_by_customer
            case Order.ServiceCallStatus.COMPLETED:
                return Str.order_service_call_status_completed
            default:
                return Str.order_status_unknown
            }
        }
    }
    
    private static let PropPicture = "default_picture"
    private static let PropAddress = "address"
    private static let PropLocationDescription = "location"
    private static let PropUrgent = "urgent"
    
    
    var picture: String? {
        set { self.setCustom(Order.PropPicture, value: newValue!)}
        get { return self.getCustom(Order.PropPicture) as? String}
    }
    
    var address: HKAddress? {
        set { self.setCustom(Order.PropAddress, value: newValue!.toDict()) }
        get {
            if let value = self.getCustom(Order.PropAddress) as? [String: String]  {
                return HKAddress.init(fromDict: value)
            } else {
                return nil
            }
        }
    }
    var locationName: String? {
        get {
            return self.address?.fullAddress() ?? String(format: "%.5f, %.5f", self.latitude, self.longitude)
        }
    }
    
    var locationDescription: String? {
        set { self.setCustom(Order.PropLocationDescription, value: newValue!)}
        get { return self.getCustom(Order.PropLocationDescription) as? String}
    }
    
    var isUrgent: Bool? {
        set { self.setCustom(Order.PropUrgent, value: newValue!)}
        get { return self.getCustom(Order.PropUrgent) as? Bool}
    }
    
    func isOwnedByCurrentUser() -> Bool {
//        HonkioApi.activeUser()?.userId
        return true
    }

}
