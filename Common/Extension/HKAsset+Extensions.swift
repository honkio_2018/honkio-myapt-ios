//
//  HKAsset+Extensions.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/14/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

extension HKAsset {

    static let Structure = (
        ROOM: "5c89019f6c1c6a4dd7fc59d1",
        SAUNA: "5c8900746c1c6a4da16685b5"
    )
    
    static let AllStructures = [ Structure.ROOM, Structure.SAUNA ]
    
    private static let FieldAddress = "address"
    
    func applyProperty(_ key: String, _ value: Any?) {
        if self.properties == nil {
            self.properties = [:]
        }
        
        self.properties[key] = value
    }
    
    var address: HKAddress? {
        set {
            self.applyProperty(HKAsset.FieldAddress, newValue?.toDict())
        }
        get {
            if let value = self.properties?[HKAsset.FieldAddress] as? [String: String]  {
                return HKAddress.init(fromDict: value)
            } else {
                return nil
            }
        }
    }
}
