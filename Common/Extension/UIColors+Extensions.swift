//
//  UIColors+Extensions.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/22/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setSolidBackground() {
        self.backgroundColor = UIColor(netHex: AppColors.MainColor)
        self.setTitleColor(UIColor(netHex: AppColors.MainTextColor), for: UIControl.State.normal)
        self.setTitleColor(UIColor(netHex: AppColors.MainColor), for: UIControl.State.highlighted)
    }
    
}
