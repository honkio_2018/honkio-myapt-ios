//
//  AppApi.swift
//  BookAHorse
//
//  Created by Mikhail Li on 16/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

open class AppApi {

    public fileprivate(set) static var instance : AppApiInstance!
    
    public static func initInstance(instance: AppApiInstance) {
        AppApi.instance = instance
    }
    
}
