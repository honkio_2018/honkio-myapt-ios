//
//  AppApiInstance.swift
//  BookAHorse
//
//  Created by Mikhail Li on 16/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

public class AppApiInstance {
    
    
    public private(set) var assetStructures = [String: HKStructure]()
    public private(set) var roleStructures = [String: HKUserRoleStructure]()
    
    public init() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.userStructureList(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_ROLES_DESCRIPTIONS)),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.assetStructureList(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_ASSET_STRUCTURE)),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.logout(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_LOGOUT)),
                                               object: nil)
    }
    
    private func getResponseFromNotification(_ notification: Notification) -> Response? {
        guard let userInfo = notification.userInfo as? [String: AnyObject?] else { return nil }
        guard let result = userInfo[NOTIFY_EXTRA_RESULT] as? Response else { return nil }
        
        return result
    }
    
    @objc public func userStructureList(_ notification: Notification) {
        guard let result = getResponseFromNotification(notification) else { return }
        guard let structureList = result.result as? HKUserRoleStructureList else { return }
        
        onStructureGet(structures: structureList)
    }
    
    @objc public func assetStructureList(_ notification: Notification) {
        guard let result = getResponseFromNotification(notification) else { return }
        guard let structureList = result.result as? HKStructureList else { return }
        
        onStructureGet(structures: structureList)
    }
    
    @objc public func logout(_ notification: Notification) {

    }
    
    public func onStructureGet(structures: HKUserRoleStructureList) {
        for item in structures.list {
            onStructureGet(structure: item)
        }
    }
    
    public func onStructureGet(structures: HKStructureList) {
        for item in structures.list {
            onStructureGet(structure: item)
        }
    }
 
    public func onStructureGet(structure: HKStructure) {
        assetStructures[structure.objectId] = structure
    }
    
    public func onStructureGet(structure: HKUserRoleStructure) {
        roleStructures[structure.objectId] = structure
    }
}
