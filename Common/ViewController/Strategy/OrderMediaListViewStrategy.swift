//
//  AssetMediaListViewStrategy.swift
//  honkio-indx-ios
//
//  Created by Shurygin Denis on 2/28/19.
//  Copyright © 2019 RiskPointer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OrderMediaListViewStrategy: HKOrderMediaListViewStrategy {
    
    override func setViews(_ collection: UICollectionView, _ progress: UIView?) {
        super.setViews(collection, progress)
        
        collection.register(UINib(nibName: "HKMediaFileTableViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "HKMediaFileTableViewCell")
        collection.register(UINib(nibName: "HKMediaFileAddTableViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "HKMediaFileAddTableViewCell")
    }
}
