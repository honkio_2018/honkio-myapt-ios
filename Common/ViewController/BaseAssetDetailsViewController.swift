//
//  BaseAssetDetailsViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/26/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class BaseAssetDetailsViewController: HKBaseTableViewController, HKAssetViewProtocol {
    
    let TagLocation = "Location"
    
    @IBOutlet weak var tableView: UITableView!
    
    private var assetId: String?
    var asset: HKAsset? {
        didSet {
            self.assetId = asset?.assetId
            self.galleryStrategy?.asset = asset
            
            self.refresh()
        }
    }
    
    private var galleryStrategy: AssetMediaListViewStrategy?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.galleryStrategy = getLifeCycleObserver(AssetMediaListViewStrategy.TAG) as? AssetMediaListViewStrategy
        if self.galleryStrategy == nil {
            let strategy = AssetMediaListViewStrategy(AssetMediaListViewStrategy.TAG)
            strategy.asset = self.asset
            strategy.isReadOnly = true
            strategy.selectionDelegate = {[weak self] (item) in
                self?.showMediaFile(item)
            }
            strategy.addActionDelegate = {[weak self] in
                if let strongSelf = self {
                    if let viewController = AppController.instance.getViewControllerByIdentifier("MediaPickViewController", viewController: strongSelf) as? HKBaseMediaPickViewController {
                        viewController.mediaType = HK_MEDIA_FILE_TYPE_IMAGE
                        viewController.pickDelegate = strongSelf.galleryStrategy
                        
                        strongSelf.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
            }
            self.addLifeCycleObserver(strategy)
            
            self.galleryStrategy = strategy
        }
    }
    
    func loadAsset(_ assetId: String) {
        HonkioApi.userGetAsset(assetId, flags: 0) { [unowned self] (response) in
            if response.isStatusAccept() {
                self.asset = response.result as? HKAsset
            }
        }
    }
    
    func actionShowLocation() {
        if let controller = AppController.instance.getViewControllerByIdentifier("MapViewController", viewController: self) as? MapViewController {

            if let asset = self.asset {
                controller.location = CLLocation(latitude: asset.latitude, longitude: asset.longitude)
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let tag = (self.sourceItem(indexPath)?.item as? (title: String, value: String?, tag: String?))?.tag {
            self.onItemClick(tag)
        }
    }
    
    func onItemClick(_ tag: String) {
        switch tag {
        case TagLocation:
            self.actionShowLocation()
            break
        default:
            break // Do nothing
        }
    }
    
    private func showMediaFile(_ mediaFile: HKMediaFile) {
        if let viewController = AppController.getViewControllerById(AppController.MediaFiles, viewController: self) as? HKMediaPagesViewDelegate {
            viewController.mediaList = self.galleryStrategy?.mediaList
            viewController.initialPage = self.galleryStrategy?.mediaList?.firstIndex(of: mediaFile) ?? 0
            self.navigationController?.pushViewController(viewController as! UIViewController, animated: true)
        }
    }
    
    open func refresh() {
        self.itemsSource.removeAll()
        self.initDataSource()
        self.tableView?.reloadData()
    }
    
    // MARK: Data initialization
    open func initDataSource() {
        
        guard self.asset != nil else { return }
        
        let section = HKTableViewSection()
        
        // Media files
        section.items.append((MediaGalleryTableCell.Identifier, self.galleryStrategy, {(viewController, cell, item) in
            let mediaCell = cell as! MediaGalleryTableCell
            (item as? HKBaseMediaListViewStrategy)?.setViews(mediaCell.collectionView, mediaCell.progressView)
        }))
        
        self.itemsSource.append(section)
    }
}
