//
//  BaseEditAssetViewController.swift
//  honkio-indx-ios
//
//  Created by Shurygin Denis on 12/7/18.
//  Copyright © 2018 RiskPointer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class BaseEditOrderServiceCallViewController: HKBaseOrderViewController {
    
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var labelLocation: UILabel!
//    @IBOutlet weak var textCategoryC: UITextField!
    
//    @IBOutlet weak var buttonCategoryA: UIButton!
//    @IBOutlet weak var buttonCategoryB: UIButton!
    
    @IBOutlet weak var galleryProgressView: UIView!
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    
//    public var order: Order? {
//        didSet {
//            self.orderDidSet(self.order)
//        }
//    }
//    
//    private var categoryA: [String]?
//    private var categoryB: [String]?
    
    private var locationLat: Double?
    private var locationLon: Double?
    private var address: HKAddress?
    
    let emptyTextValidator = HKTextValidator(errorMessage: "text_validator_field_empty".localized, rool: HKTextValidator.ruleNotEmpty())
    
    open func getStructure() -> HKStructure? {
        return nil
    }
    
    private var galleryStrategy: OrderMediaListViewStrategy?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
//        self.textName.delegate = self
        
//        // FIXME: copypase
//        self.galleryStrategy = getLifeCycleObserver(OrderMediaListViewStrategy.TAG) as? OrderMediaListViewStrategy
//        if self.galleryStrategy == nil {
//            let strategy = OrderMediaListViewStrategy(OrderMediaListViewStrategy.TAG)
//            strategy.setViews(self.galleryCollectionView, self.galleryProgressView)
//            strategy.order = self.order
//            strategy.selectionDelegate = {[weak self] (item) in
//                let isOwner = true
////                    self?.order?.isOwnedByCurrentUser() ?? false
//                
//                if isOwner {
//                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//                    alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
//                    
//                    alertController.addAction(UIAlertAction(title: "menu_media_show".localized, style: .default) { [weak self] action in
//                        self?.showMediaFile(item)
//                    })
//                    
//                    alertController.addAction(UIAlertAction(title: "menu_media_remove_file".localized, style: .default) { [weak self] action in
//                        self?.galleryStrategy?.removeMediaFile(item)
//                    })
//                    
//                    self?.present(alertController, animated: true)
//                }
//                else {
//                    self?.showMediaFile(item)
//                }
//            }
//            strategy.addActionDelegate = {[weak self] in
//                if let strongSelf = self {
//                    if let viewController = AppController.instance.getViewControllerByIdentifier("MediaPickViewController", viewController: strongSelf) as? HKBaseMediaPickViewController {
//                        viewController.mediaType = HK_MEDIA_FILE_TYPE_IMAGE
//                        viewController.pickDelegate = strongSelf.galleryStrategy
//                        
//                        strongSelf.navigationController?.pushViewController(viewController, animated: true)
//                    }
//                }
//            }
//            self.addLifeCycleObserver(strategy)
//            
//            self.galleryStrategy = strategy
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        if self.order?.orderId != nil {
//            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_delete_indx"), style: .plain, target: self, action: #selector(EditAssetProductViewController.actionDelete(_:)))
//        }
//        else {
//            self.navigationItem.rightBarButtonItems = []
//        }
    }
    
//    @IBAction open func actionDelete(_ sender: Any?) {
//        var orderTypeName: String?
//
//        if self.order?.structureIs(AppApi.ASSET_PRODUCT) ?? false {
//            orderTypeName = "screen_asset_details_product".localized
//        }
//        else if self.asset?.structureIs(AppApi.ASSET_EVENT) ?? false {
//            assetTypeName = "screen_asset_details_event".localized
//        }
//        else {
//            assetTypeName = ""
//        }
//
//        CustomAlertView(okTitle: String(format: "screen_asset_details_msg_delete_asset_title".localized, assetTypeName!), okMessage: String(format: "screen_asset_details_msg_delete_asset_body".localized, assetTypeName!), okAction: {[unowned self] in
//
//            BusyIndicator.shared.showProgressView(self.view)
//            HonkioMerchantApi.merchantAssetDelete(self.asset, shop: AppApi.instance.userShop, flags: 0, handler: {[weak self] (response) in
//                BusyIndicator.shared.hideProgressView()
//                if response.isStatusAccept() {
//                    self?.navigationController?.popViewController(animated: false)
//                }
//                else if let strongSelf = self {
//                    let error = response.anyError()
//                    if !AppErrors.handleError(strongSelf, error: error) {
//                        CustomAlertView(apiError: error, okAction: nil).show()
//                    }
//                }
//            })
//
//
//        }).addButtonWithTitle("dlg_common_button_cancel".localized, action: {
//            // do nothing
//        }).show()
//    }
    
//    @IBAction open func actionSave(_ sender: Any?) {
//        if self.isValid() {
//            let order = Order(from: self.order)!
//
//            self.fillOrder(order)
//
//            BusyIndicator.shared.showProgressView(self.view)
//            HonkioMerchantApi.merchantAssetSet(order, shop: AppApi.instance.userShop, flags: 0) { [weak self] (response) in
//                if let strongSelf = self {
//                    if response.isStatusAccept() {
//                        self?.assetDidSaved(response.result as! HKAsset)
//                    }
//                    else {
//                        BusyIndicator.shared.hideProgressView()
//                        let error = response.anyError()
//                        if !AppErrors.handleError(strongSelf, error: error) {
//                            CustomAlertView(apiError: error, okAction: nil).show()
//                        }
//                    }
//                }
//            }
//        }
//    }
    
 

    
    open func orderDidSet(_ order: Order?) {
        self.locationLat = order?.latitude
        self.locationLon = order?.longitude
        self.address = order?.address
        
        self.galleryStrategy?.order = order
        
        self.updateUI()
    }
    
    open func fillOrder(_ order: Order) {
//        order.isVisible = true
        order.longitude = self.locationLon ?? 0.0
        order.latitude = self.locationLat ?? 0.0
//        order.address = self.address
//        order.ownerUser = HonkioApi.activeUser()?.userId
//        order.ownerShop = AppApi.instance.userShop?.identityId
        
//        order.name = self.textName.text
//        asset.categoryC = self.textCategoryC.text
//
//        asset.categoryA = self.categoryA
//        asset.categoryB = self.categoryB
//
//        asset.stricture = getStructure()
    }
    
    @IBAction func actionSetLocation() {
        if let viewController = AppController.instance.getViewControllerByIdentifier("MapViewController", viewController: self) as? MapViewController {
            
            viewController.title = "screen_select_on_map_title".localized
            viewController.readOnly = false
            
            if let lat = self.locationLat, let lon = self.locationLon {
                viewController.location = CLLocation(latitude: lat, longitude: lon)
            }
            
            viewController.completeAction = { [weak self] (viewController: MapViewController, place: HKAddress?, location: CLLocation?) in
                self?.locationLat = location?.coordinate.latitude
                self?.locationLon = location?.coordinate.longitude
                self?.address = place
                self?.updateLocationView()
                self?.navigationController?.popViewController(animated: true)
            }
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    open func isValid() -> Bool {
        var isValid = true
        
        isValid = emptyTextValidator.validate(self.textName) && isValid
        
        return isValid
    }
    
    open func updateUI() {
//        self.textName.text = self.asset?.name
//        self.textCategoryC.text = asset?.categoryC
        self.updateLocationView()
//        self.updateCategoriesUI()
    }
    
    private func updateLocationView() {
        if !self.isLocationSet() {
            self.labelLocation.text = "screen_edit_asset_location_not_set".localized
        }
        else {
            self.labelLocation.text = self.address?.fullAddress() ?? String(format: "%.5f, %.5f", self.locationLat ?? 0.0, self.locationLon ?? 0.0)
        }
    }
    
//    private func updateCategoriesUI() {
//        self.buttonCategoryA.setTitle(self.getNameFor(self.categoryA, HKAsset.PropCategoryA) ?? "screen_edit_asset_category_not_set".localized, for: .normal)
//        self.buttonCategoryB.setTitle(self.getNameFor(self.categoryB, HKAsset.PropCategoryB) ?? "screen_edit_asset_category_not_set".localized, for: .normal)
//    }
    
//    private func getNameFor(_ value: [String]?, _ field: String) -> String? {
//        let structure = AppApi.instance.assetStructures[AppApi.ASSET_EVENT]
//        return value?.map { (value) -> String in
//            return structure?.findEnumPropertyValue(field, value: value)?.name ?? value
//            }.joined(separator: ", ")
//    }
    
    private func isLocationSet() -> Bool {
        return self.locationLat != nil && self.locationLon != nil && !(self.locationLat == 0 && self.locationLon == 0)
    }
    
    private func orderDidSaved(_ order: Order) {
        self.galleryStrategy?.saveDeferred(order.orderId, {[weak self] (error) in
            BusyIndicator.shared.hideProgressView()
            if let strongSelf = self {
                if error != nil {
                    if !AppErrors.handleError(strongSelf, error: error) {
                        CustomAlertView(apiError: error, okAction: nil).show()
                    }
                }
                else {
                    let navigationController = self?.navigationController
                    
                    if let viewController = AppController.getViewControllerById(AppController.OrderDetails, viewController: strongSelf) as? HKOrderViewProtocol {
                        navigationController?.popViewController(animated: false)
                        if strongSelf.order?.orderId != nil {
                            navigationController?.popViewController(animated: false)
                        }
                        viewController.order = order
                        navigationController?.pushViewController(viewController as! UIViewController, animated: true)
                    }
                }
            }
        })
    }
    
    
    //===============================
    // FIXME: copypaste
    
    private func showMediaFile(_ mediaFile: HKMediaFile) {
        if let viewController = AppController.getViewControllerById(AppController.MediaFiles, viewController: self) as? HKMediaPagesViewDelegate {
            viewController.mediaList = self.galleryStrategy?.mediaList
            viewController.initialPage = self.galleryStrategy?.mediaList?.firstIndex(of: mediaFile) ?? 0
            self.navigationController?.pushViewController(viewController as! UIViewController, animated: true)
        }
    }
}
