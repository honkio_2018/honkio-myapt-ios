//
//  MainTabViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/14/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class MainTabViewController: UITabBarController, UINavigationControllerDelegate {
    
    var tabBarVC : UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarVC = self.moreNavigationController.topViewController
        self.moreNavigationController.delegate = self;
        
    }

    @IBAction func actionMainMenu() {
        if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if( viewController == tabBarVC ){
            navigationController.navigationBar.topItem?.rightBarButtonItem = nil
            moreNavigationController.navigationBar.topItem?.title = "app_name".localized(table: Target.LocalizableTable)
        }
    }
}
