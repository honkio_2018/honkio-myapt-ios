//
//  ChargeViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/25/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class ChargeViewController: HKBaseViewController {
    
    private static let ChargeStratTime = "App.ChargeViewController.ChargeStratTime"
    private static let ChargeEndTime = "App.ChargeViewController.ChargeEndTime"
    
    
    private static let KWT_FOR_1_SEK: Float = 0.001111
    private static let AMOUNT_FOR_1_SEK: Float = 0.000555
    private static let VAT_PERCENT: Float = 10
    private static let VAT_DIVIDER: Float = 1.0 + (VAT_PERCENT / 100.0)
    
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelStartTime: UILabel!
    @IBOutlet weak var labelStopTime: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelPaid: UILabel!
    @IBOutlet weak var labelVat: UILabel!
    @IBOutlet weak var buttonAction: UIButton!
    
    private let dateFormatter = DateFormatter()
    
    private var startTime: Date?
    private var endTime: Date?
    private var timer: Timer?

    @IBAction func actionMainMenu() {
        if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "app_name".localized(table: Target.LocalizableTable)
        
        self.dateFormatter.setLocalizedDateFormatFromTemplate("d/M-y")
        self.dateFormatter.dateStyle = .short
        self.dateFormatter.timeStyle = .short
        
        self.startTime = StoreData.load(fromStore: ChargeViewController.ChargeStratTime) as? Date
        self.endTime = StoreData.load(fromStore: ChargeViewController.ChargeEndTime) as? Date
        
        self.buttonAction.layer.cornerRadius = 5
        self.buttonAction.setSolidBackground()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateTimerLabel()
        self.updateTimerButton()
        
        if self.startTime != nil && self.endTime == nil {
            self.scheduleTimer()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopScheduleTimer()
    }
    
    @IBAction func actionTimer() {
        if self.startTime == nil || self.endTime != nil {
            self.startTime = Date()
            self.endTime = nil
            StoreData.save(toStore: self.startTime, forKey: ChargeViewController.ChargeStratTime)
            StoreData.delete(fromStore: ChargeViewController.ChargeEndTime)
            self.updateTimerLabel()
            self.scheduleTimer()
        }
        else {
            self.endTime = Date()
            StoreData.save(toStore: self.endTime, forKey: ChargeViewController.ChargeEndTime)
            self.updateTimerLabel()
            self.stopScheduleTimer()
        }
        self.updateTimerButton()
    }
    
    @objc private func updateTimerLabel() {
        var duration: Int = 0
        if let startTime = self.startTime {
            self.labelStartTime.text = self.dateFormatter.string(from: startTime)
            
            if let endTime = self.endTime {
                self.labelStopTime.text = self.dateFormatter.string(from: endTime)
                duration = Int(endTime.timeIntervalSince(startTime))
            } else {
                self.labelStopTime.text = "-"
                duration = Int(Date().timeIntervalSince(startTime))
            }
        }
        else {
            self.labelStartTime.text = "-"
            self.labelStopTime.text = "-"
        }
        
        let minutes = duration / 60
        let seconds = duration % 60
        
        self.labelTime.text = String(format: "%02i:%02i", minutes, seconds)
        
        let paidAmount = Float(duration) * ChargeViewController.AMOUNT_FOR_1_SEK
        let vatAmount = paidAmount - (paidAmount / ChargeViewController.VAT_DIVIDER)
        
        self.labelTotal.text = String(format: "%.6f", Float(duration) * ChargeViewController.KWT_FOR_1_SEK)
        self.labelPaid.text = String(format: "%.2f EUR", paidAmount)
        self.labelVat.text = String(format: "%.2f EUR", vatAmount)
        
    }
    
    private func scheduleTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ChargeViewController.updateTimerLabel), userInfo: nil, repeats: true)
    }
    
    private func stopScheduleTimer() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    private func updateTimerButton() {
        if self.startTime == nil || self.endTime != nil {
            self.buttonAction.setTitle(Str.screen_charge_car_button_start, for: .normal)
        }
        else {
            self.buttonAction.setTitle(Str.screen_charge_car_button_stop, for: .normal)
        }
    }
}
