//
//  BookOrderDetailsViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/22/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class BookOrderDetailsViewController: HKBaseTableViewController, HKOrderViewProtocol, HKOrderSetVCStrategyDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var buttonOrderAction1: UIButton!
    
    private let TagOrderPublishing = "OrderPublishing"
    private let TagOrderApproveByParent = "OrderApproveByParent"
    
    public var order: Order! {
        didSet {
            self.shop = nil
            self.asset = nil
            
            if self.order.shopId != nil {
                HonkioApi.shopFind(byId: self.order.shopId, flags: 0) {[weak self] (response) in
                    if response.isStatusAccept() {
                        let shopFind = response.result as! ShopFind
                        if shopFind.list.count > 0 {
                            self?.shop = shopFind.list.first
                        }
                    }
                }
            }
            
            if self.order.asset != nil {
                HonkioApi.userGetAsset(self.order.asset, flags: 0) {[weak self] (response) in
                    if response.isStatusAccept() {
                        self?.asset = response.result as? HKAsset
                    }
                }
            }
            
            if self.order != nil {
                HonkioApi.userSetPush(true, filter: PushFilter().addContent(PUSH_FILTER_QUERY_CONTENT_ORDER, value: self.order.orderId), flags: MSG_FLAG_LOW_PRIORITY + MSG_FLAG_NO_WARNING, handler: nil)
            }
            
            self.initDataSource()
            self.tableView.reloadData()
            self.setupActionViews()
        }
    }
    
    private var shop: Shop? {
        didSet {
            if self.shop != nil {
                self.initDataSource()
                self.tableView.reloadData()
            }
        }
    }
    
    private var asset: HKAsset? {
        didSet {
            if self.asset != nil {
                self.initDataSource()
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 50
        
        self.buttonOrderAction1.setSolidBackground()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.itemsSource[indexPath.section].items[indexPath.row].identifierOrNibName == String(describing: OrderHeaderViewCell.self) {
            let controller = AssetDetailsViewController()
            
            if self.asset != nil {
                controller.asset = self.asset
            } else {
                controller.loadAsset(self.order.asset)
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    // MARK: - HKOrderSetVCStrategyDelegate protocol
    
    func orderWillSet(_ vcStrategy: HKSimpleOrderSetVCStrategy) {
        BusyIndicator.shared.showProgressView(self.view)
    }
    
    func orderDidSet(_ vcStrategy: HKSimpleOrderSetVCStrategy, order: Order) {
        BusyIndicator.shared.hideProgressView()
        if vcStrategy.tag == TagOrderPublishing {
            CustomAlertView(okTitle: Str.screen_order_details_dialog_order_published_title, okMessage: Str.screen_order_details_dialog_order_published_message) {
                    [unowned self] in
                    if let viewControllers = self.navigationController?.viewControllers {
                        self.navigationController?.viewControllers = [viewControllers.first!, viewControllers.last!]
                    }
                }.show()
        }
        self.order = order
    }
    
    func onOrderSetError(_ vcStrategy: HKSimpleOrderSetVCStrategy, error: ApiError) {
        BusyIndicator.shared.hideProgressView()
        if !AppErrors.handleError(self, error: error) {
            if error.code == 3305 || error.code == ErrUserOrder.timeReservationError.rawValue {
                CustomAlertView(okTitle: Str.screen_order_details_dlg_error_reserving_time_title, okMessage: Str.screen_order_details_dlg_error_reserving_time_message, okAction: nil).show()
            }
            else if error.code == ErrUserOrder.paymentError.rawValue {
                CustomAlertView(okTitle: Str.screen_order_details_dlg_error_payment_title, okMessage: Str.screen_order_details_dlg_error_payment_message, okAction: nil).show()
            }
            else {
                CustomAlertView(apiError: error, okAction: nil).show()
            }
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func orderAction(_ sender: Any?) {
        if order.orderId == nil || order.status?.isEmpty ?? true {
            self.actionOrderPublish()
        } else {
            
        }
    }
    
    private func actionOrderPublish() {
        moveOrder(self.order, toStatus: self.order.status, tag: TagOrderPublishing, askAccount: true)
    }
    
    private func setupActionViews() {
        if order.orderId == nil || order.status?.isEmpty ?? true {
            self.buttonOrderAction1.isHidden = false
            self.buttonOrderAction1.setTitle(Str.screen_order_details_confirm, for: .normal)
        } else {
            self.buttonOrderAction1.isHidden = true
        }
    }
    
    private func initDataSource() {
//        let user = HonkioApi.activeUser()
        
        self.itemsSource.removeAll()
        
        if let order = self.order {
            let asset = self.asset
            let section = HKTableViewSection()
            
            let isNewOrder = order.orderId == nil || order.status?.isEmpty ?? true
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .short
            
            section.items.append((String(describing: OrderHeaderViewCell.self), self.asset, {(viewController, cell, item) in
                    (cell as! OrderHeaderViewCell).asset = item as? HKAsset
                }))
            
            // Chat Button
            section.items.append((String(describing: ChatButtonTableViewCell.self), order.title, {(viewController, cell, item) in
                (cell as! ChatButtonTableViewCell).orderDetailsVC = self
            }))
            
            // Booking status
            if isNewOrder {
                section.items.append(DetailedItemCell
                    .buildSourceItem(Str.screen_order_details_status, Str.order_status_book_request))
            } else {
                section.items.append(DetailedItemCell
                    .buildSourceItem(Str.screen_order_details_status, order.statusName))
            }
            
            if isNewOrder {
                section.items.append(DetailedItemCell
                    .buildSourceItem(Str.screen_order_details_order_creator_name, HonkioApi.activeUser()?.name))
            } else {
                section.items.append(DetailedItemCell
                    .buildSourceItem(Str.screen_order_details_order_creator_name, order.userOwner))
            }
            
            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_details_asset_name, asset?.name ?? "..."))
            
            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_details_creation_date, dateFormatter.string(from: order.creationDate)))
            
            
            dateFormatter.timeStyle = .none
            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_details_date, dateFormatter.string(from: order.startDate)))
            
            dateFormatter.dateStyle = .none
            dateFormatter.timeStyle = .short
            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_details_start_time, dateFormatter.string(from: order.startDate)))
            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_details_end_time, dateFormatter.string(from: order.endDate)))

            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_details_product, productsName(order.products)))

            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_details_amount, productsPrice(order.products)))

            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_details_comments, order.comment))

            self.itemsSource.append(section)
        }
    }
    
    private func moveOrder(_ order: Order, toStatus: String, tag: String?, askAccount: Bool = true) {
        let newOrder = Order(from: order)!
        newOrder.status = toStatus
        
        let currentUserId = HonkioApi.activeUser()?.userId
        
        let orderSetStrategy = HKOrderSetVCStrategy(tag, parent: self)
        orderSetStrategy.removeOnCompletion = true
        orderSetStrategy.isPinRequired = true
        orderSetStrategy.isAccountRequired = askAccount
        orderSetStrategy.useMerchantApi = order.userOwner != currentUserId && order.thirdPerson != currentUserId
        orderSetStrategy.userSetOrder(newOrder, flags: 0)
    }
    
    private func productsName(_ products: [BookedProduct]?) -> String {
        if products == nil || products!.count == 0 {
            return "-"
        }
        
        var result = ""
        for booking in products! {
            if result.count > 0 {
                result += ", "
            }
            
            if booking.count > 1 {
                result += "\(booking.product.name ?? "null")(\(booking.count))"
            } else {
                result += "\(booking.product.name ?? "null")"
            }
        }
        
        return result
    }
    
    private func productsPrice(_ products: [BookedProduct]?) -> String {
        if products == nil || products!.count == 0 {
            return "-"
        }
        
        var price = 0.0
        var currency: String?
        
        for booking in products! {
            price += booking.getPrice()
            
            if currency == nil {
                currency = booking.product.currency
            }
        }
        
        return String(format: "%.2f %@", price, currency ?? "")
    }
}
