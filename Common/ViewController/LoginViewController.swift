//
//  LoginViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 13/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

class LoginViewController: HKOAuthLoginViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.splashView.backgroundColor = UIColor(netHex: AppColors.MainColor)
    }
    
    override func serverDataDidLoad(doLogin: Bool) {
        HonkioApi.assetStructureList(HKAsset.AllStructures, shop: nil, flags: MSG_FLAG_NO_WARNING, handler: nil)
        super.serverDataDidLoad(doLogin: doLogin)
    }
    
}
