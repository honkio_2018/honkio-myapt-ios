//
//  AssetProductsViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/14/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class AssetProductsViewController: HKBaseScheduleProductsTableViewController, HKAssetViewProtocol, HKLoginViewLoginDelegate {
    
    private static let LCO_TAG_INTEREST = "LCO_TAG_INTEREST"
    
    public var asset: HKAsset? {
        didSet {
            self.updateHeader()
        }
    }
    
    func loadAsset(_ assetId: String) {
        self.filter.objectId = assetId
        HonkioApi.userGetAsset(assetId, flags: 0) {[weak self] (response) in
            if response.isStatusAccept() {
                self?.asset = response.result as? HKAsset
            }
        }
    }
    
    private let headerSection = HKTableViewSection()
    
    let horseDetailsCell = "HorseDetailsCell"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }
    
    override func buildSource(_ lazySection: HKTableViewSection) {
        self.itemsSource.removeAll()
        self.itemsSource.append(self.headerSection)
        self.itemsSource.append(lazySection)
        self.updateHeader()
    }
    
    override func cellIdentifier(_ item: Product) -> String {
        return "ProductItemCell"
    }
    
    override func setupFilter(_ filter: ScheduleFilter, count: Int, skip: Int) {
        super.setupFilter(filter, count: count, skip: skip)
        
        self.filter.objectId = self.asset?.assetId ?? self.filter.objectId
        self.filter.objectType = HK_OBJECT_TYPE_ASSET
        self.filter.startDate = Date()
        
        var dateComp = DateComponents()
        dateComp.month = 3
        self.filter.endDate = Calendar.current.date(byAdding: dateComp, to: self.filter.startDate)!
    }
    
    override func loadFinished() {
        super.loadFinished()
        
//        if self.lazyItemsCount() == 0 {
//            self.lazySection.items.append(("HorseDetailsEmptyCell", nil, { (viewController, cell, item) in
//                let emptyCell = cell as! HorseDetailsEmptyCell
//
//                emptyCell.btnInterest.removeTarget(viewController, action: #selector(HorseProductsViewController.actionInterest(_:)), for: .touchUpInside)
//                emptyCell.btnInterest.addTarget(viewController, action: #selector(HorseProductsViewController.actionInterest(_:)), for: .touchUpInside)
//            }))
//            self.tableView.reloadData()
//        }
    }
    
    override func buildCellBunder() -> ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)? {
        return { (viewController, cell, item) in
            let productCell = cell as! ProductItemCell
            let product = item as! Product
            
            productCell.labelName.text = product.name
            productCell.labelDesc.text = product.desc
            productCell.labelAmount.text = String(format: "%.2f %@", arguments: [product.amount, product.currency ?? "null"])
        }
    }
    
    func buildHeaderCellBunder() -> ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)? {
        return { (viewController, cell, item) in
            (cell as? AssetTableViewCell)?.asset = item as? HKAsset
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if self.isLasySection(self.itemsSource[indexPath.section]) {
            if !HonkioApi.isConnected() {
//                let alertController = UIAlertController(title: Str.screen_horses_list_dialog_not_logged_in_title,
//                                                        message: Str.screen_horses_list_dialog_not_logged_in_message,
//                                                        preferredStyle: .alert)
//                let cancelAction = UIAlertAction(title: Str.screen_horses_list_dialog_not_logged_in_cancel, style: .cancel)
//                alertController.addAction(cancelAction)
//                let loginAction = UIAlertAction(title: "Login", style: .default) { [unowned self](action: UIAlertAction) in
//                    print("Login pressed");
//                    if let viewController = AppController.instance.getViewControllerById(AppController.Login, viewController: self) as? HKLoginViewProtocol {
//                        viewController.doLogin = false
//                        viewController.loginDelegate = self
//                        self.navigationController?.pushViewController(viewController as! UIViewController, animated: true)
//                    }
//                }
//                alertController.addAction(loginAction)
//                self.present(alertController, animated: true, completion: nil)
            }
            else if let product = self.lazyItem(indexPath) as? Product, HonkioApi.isConnected() {
                if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: AssetEventsListViewController.self), viewController: self) as? AssetEventsListViewController {
                    controller.products =  [BookedProduct(product: product, andCount: 1)]
                    controller.asset = self.asset
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
        else if indexPath.section == 0 && indexPath.row == 0 {
            if let asset = self.asset {
                let controller = AssetDetailsViewController()
                controller.loadAsset(asset.assetId)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    private func updateHeader() {
        self.headerSection.items.removeAll()
        if self.asset == nil {
            self.headerSection.items.append(("ProgressCell", nil, nil))
        }
        else {
            self.headerSection.items.append(("AssetTableViewCell", self.asset, self.buildHeaderCellBunder()))
        }
    }
    
    // MARK: - HKLoginViewLoginDelegate
    
    func loginFinished(_ view: HKLoginViewProtocol, _ isLogedin: Bool) {
        if isLogedin {
            // Restart main screen if logedin
            if let viewController = AppController.getViewControllerById(AppController.Main, viewController: self) {
                
                if let navigationVC = viewController as? UINavigationController {
                    if let thisVC = AppController.instance.getViewControllerByIdentifier(String(describing: AssetProductsViewController.self), viewController: self) as? HKAssetViewProtocol {
                        thisVC.asset = self.asset
                        navigationVC.pushViewController(thisVC as! UIViewController, animated: false)
                    }
                }
                
                let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
                appDelegate.changeRootViewController(viewController)
            }
        }
        else if let delegate = view as? HKLoginViewLoginDelegate {
            delegate.loginFinished(view, isLogedin)
        }
    }
}
