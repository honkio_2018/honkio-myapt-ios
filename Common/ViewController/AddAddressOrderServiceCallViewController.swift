//
//  EditAssetProductViewController.swift
//  honkio-indx-ios
//
//  Created by Shurygin Denis on 12/5/18.
//  Copyright © 2018 RiskPointer. All rights reserved.
//

import UIKit
import HonkioAppTemplate
//HKBaseOrderViewController
//HKWizardViewController

class AddAddressOrderServiceCallViewController: HKWizardViewController {
    
    @IBOutlet weak var addresView: AddressView!
    
    private var isInit: Bool = false
    private var address: HKAddress?
    var order: Order?
    
    override func viewDidLoad() {
        self.addresView.parentViewController = self
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(AddOrderServiceCallViewController.actionNext))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInit {
            if let orderAddress = self.wizardNavigationController?.wizardObject as? Order {
                self.addresView.setAddress(orderAddress.address)
            }
            isInit = true
        }
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func actionNext(_ sender: Any?) {
        BusyIndicator.shared.showProgressView(self.view)
        self.addresView.syncAddress { [unowned self] in
            BusyIndicator.shared.hideProgressView()

//            if let orderAddress = self.wizardNavigationController?.wizardObject as? Order {
//                orderAddress.address = self.addresView.getAddress()
//                if let location = self.addresView.location {
//                    orderAddress.longitude = location.coordinate.longitude
//                    orderAddress.latitude = location.coordinate.latitude
//                }
//            }
            
            self.order!.address = self.addresView.getAddress()
            if let location = self.addresView.location {
                self.order!.longitude = location.coordinate.longitude
                self.order!.latitude = location.coordinate.latitude
            }
            

        
        if self.isValid() {
            if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: OrderServiceCallDetailsViewController.self), viewController: self) as? OrderServiceCallDetailsViewController {
                controller.order = self.order
                
//                controller.order.longitude = self.order!.longitude
//                controller.order.latitude = self.order!.latitude
//                controller.order.address = self.order!.address
            self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
}
    
   private func isValid() -> Bool {
        return self.addresView.isValid()
    }
    
}
