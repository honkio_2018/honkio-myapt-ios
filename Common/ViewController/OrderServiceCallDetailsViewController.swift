//
//  BookOrderDetailsViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/22/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OrderServiceCallDetailsViewController: HKBaseTableViewController, HKOrderViewProtocol, HKOrderSetVCStrategyDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var buttonOrderAction1: UIButton!
    
    private let TagOrderPublishing = "OrderPublishing"
    private let TagOrderCancelling = "OrderCancelling"
    private let ORDER_SERVICE_CALL_CANCEL = "cancelled"
    
    private var galleryStrategy: OrderMediaListViewStrategy?
    
    public var order: Order! {
        didSet {
            self.galleryStrategy?.order = order
            
            if self.order != nil {
                HonkioApi.userSetPush(true, filter: PushFilter().addContent(PUSH_FILTER_QUERY_CONTENT_ORDER, value: self.order.orderId), flags: MSG_FLAG_LOW_PRIORITY + MSG_FLAG_NO_WARNING, handler: nil)
            }
            
            self.initDataSource()
            self.tableView.reloadData()
            self.setupActionViews()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 50
        
        self.buttonOrderAction1.setSolidBackground()
        
        self.galleryStrategy = getLifeCycleObserver(AssetMediaListViewStrategy.TAG) as? OrderMediaListViewStrategy
        if self.galleryStrategy == nil {
            let strategy = OrderMediaListViewStrategy(OrderMediaListViewStrategy.TAG)
            strategy.order = self.order
            
            strategy.selectionDelegate = {[weak self] (item) in
                let isOwner = self?.order?.isOwnedByCurrentUser() ?? false
                
                if isOwner {
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
                    
                    alertController.addAction(UIAlertAction(title: "menu_media_show".localized, style: .default) { [weak self] action in
                        self?.showMediaFile(item)
                    })
                    
                    alertController.addAction(UIAlertAction(title: "menu_media_remove_file".localized, style: .default) { [weak self] action in
                        self?.galleryStrategy?.removeMediaFile(item)
                    })
                    
                    self?.present(alertController, animated: true)
                }
                else {
                    self?.showMediaFile(item)
                }
            }

            strategy.addActionDelegate = {[weak self] in
                if let strongSelf = self {
                    if let viewController = AppController.instance.getViewControllerByIdentifier("MediaPickViewController", viewController: strongSelf) as? HKBaseMediaPickViewController {
                        viewController.mediaType = HK_MEDIA_FILE_TYPE_IMAGE
                        viewController.pickDelegate = strongSelf.galleryStrategy
                        
                        strongSelf.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
            }
            self.addLifeCycleObserver(strategy)
            
            self.galleryStrategy = strategy
        }
    }
    
    private func showMediaFile(_ mediaFile: HKMediaFile) {

        if let viewController = AppController.getViewControllerById(AppController.MediaFiles, viewController: self) as? HKMediaPagesViewDelegate {
            viewController.mediaList = self.galleryStrategy?.mediaList
            viewController.initialPage = self.galleryStrategy?.mediaList?.firstIndex(of: mediaFile) ?? 0
            self.navigationController?.pushViewController(viewController as! UIViewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    // MARK: - HKOrderSetVCStrategyDelegate protocol
    
    func orderWillSet(_ vcStrategy: HKSimpleOrderSetVCStrategy) {
        BusyIndicator.shared.showProgressView(self.view)
    }
    
    func orderDidSet(_ vcStrategy: HKSimpleOrderSetVCStrategy, order: Order) {
        BusyIndicator.shared.hideProgressView()
        orderDidSaved(order)
        if vcStrategy.tag == TagOrderPublishing {
            CustomAlertView(okTitle: Str.screen_order_details_dialog_order_published_title, okMessage: Str.screen_order_details_dialog_order_published_message) {
                    [unowned self] in
                    if let viewControllers = self.navigationController?.viewControllers {
                        self.navigationController?.viewControllers = [viewControllers.first!, viewControllers.last!]
                    }
                }.show()
        }
        self.order = order
    }
    
    func onOrderSetError(_ vcStrategy: HKSimpleOrderSetVCStrategy, error: ApiError) {
        BusyIndicator.shared.hideProgressView()
        if !AppErrors.handleError(self, error: error) {
            if error.code == 3305 || error.code == ErrUserOrder.timeReservationError.rawValue {
                CustomAlertView(okTitle: Str.screen_order_details_dlg_error_reserving_time_title, okMessage: Str.screen_order_details_dlg_error_reserving_time_message, okAction: nil).show()
            }
            else if error.code == ErrUserOrder.paymentError.rawValue {
                CustomAlertView(okTitle: Str.screen_order_details_dlg_error_payment_title, okMessage: Str.screen_order_details_dlg_error_payment_message, okAction: nil).show()
            }
            else {
                CustomAlertView(apiError: error, okAction: nil).show()
            }
        }
    }
    
        private func orderDidSaved(_ order: Order) {
            self.galleryStrategy?.saveDeferred(order.orderId, {[weak self] (error) in
                BusyIndicator.shared.hideProgressView()
                if let strongSelf = self {
                    if error != nil {
                        if !AppErrors.handleError(strongSelf, error: error) {
                            CustomAlertView(apiError: error, okAction: nil).show()
                        }
                    }
                    else {
                        let navigationController = self?.navigationController
                          navigationController?.popToRootViewController(animated: true)
                        }
                    }
                
            })
        }
    
    // MARK: - IBAction
    
    @IBAction func orderAction(_ sender: Any?) {
        if order.orderId == nil || order.status?.isEmpty ?? true {
            self.actionOrderPublish()
        } else {
            self.order.status = ORDER_SERVICE_CALL_CANCEL
            self.actionOrderCancel()
        }
    }
    
    private func actionOrderPublish() {
        moveOrder(self.order, toStatus: self.order.status, tag: TagOrderPublishing, askAccount: false)
    }
    
    private func actionOrderCancel() {
        moveOrder(self.order, toStatus: self.order.status, tag: TagOrderCancelling, askAccount: false)
    }
    
    private func setupActionViews() {
        if order.orderId == nil || order.status?.isEmpty ?? true {
            self.buttonOrderAction1.isHidden = false
            self.buttonOrderAction1.setTitle(Str.screen_order_service_call_details_confirm, for: .normal)
        } else if order.status != ORDER_SERVICE_CALL_CANCEL {
            self.buttonOrderAction1.isHidden = false
            self.buttonOrderAction1.setTitle(Str.screen_order_service_call_details_action_cancel, for: .normal)
        } else {
            self.buttonOrderAction1.isHidden = true
        }
    }
    
    private func initDataSource() {
//        let user = HonkioApi.activeUser()
        
        self.itemsSource.removeAll()
        
        if let order = self.order {
//            let asset = self.asset
            let section = HKTableViewSection()
            
            let isNewOrder = order.orderId == nil || order.status?.isEmpty ?? true
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .short
            
            // Media files
            section.items.append((MediaGalleryTableCell.Identifier, self.galleryStrategy, {(viewController, cell, item) in
                let mediaCell = cell as! MediaGalleryTableCell
                (item as? HKBaseMediaListViewStrategy)?.setViews(mediaCell.collectionView, mediaCell.progressView)
            }))
            
            // Service Call title
            section.items.append((String(describing: OrderServiceCallTitleViewCell.self), order.title, {(viewController, cell, item) in
                (cell as! OrderServiceCallTitleViewCell).order = order
            }))
            
            // Chat Button
            section.items.append((String(describing: ChatButtonTableViewCell.self), order.title, {(viewController, cell, item) in
                (cell as! ChatButtonTableViewCell).orderDetailsVC = self
//                (cell as! ChatButtonTableViewCell).chatOr = self
            }))

            // Booking status
            if isNewOrder {
                section.items.append(DetailedItemCell
                    .buildSourceItem(Str.screen_order_service_call_details_status, Str.order_service_call_status_new))
            } else {
                section.items.append(DetailedItemCell
                    .buildSourceItem(Str.screen_order_service_call_details_status, order.statusName))
            }

            // Created by
            if isNewOrder {
                section.items.append(DetailedItemCell
                    .buildSourceItem(Str.screen_order_service_call_details_order_creator_name, HonkioApi.activeUser()?.name))
            } else {
                section.items.append(DetailedItemCell
                    .buildSourceItem(Str.screen_order_details_order_creator_name, order.userOwner))
            }

            // Created at
            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_service_call_details_creation_date, dateFormatter.string(from: order.creationDate)))

            // Urgent
            if order.isUrgent! {
            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_service_call_details_urgent_title, Str.screen_order_service_call_details_urgent_yes))
            } else {
            section.items.append(DetailedItemCell
                    .buildSourceItem(Str.screen_order_service_call_details_urgent_title, Str.screen_order_service_call_details_urgent_no))
            }

            // Description
            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_service_call_details_description, order.desc))

            // Location
            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_service_call_details_location, order.locationDescription))

            // Address
            if ( order.longitude != 0 && order.latitude != 0 ) {
            section.items.append(DetailedItemCell
                .buildSourceItem(Str.screen_order_service_call_details_location, order.locationName))
            }
            self.itemsSource.append(section)
        }
    }
    
    private func moveOrder(_ order: Order, toStatus: String, tag: String?, askAccount: Bool = true) {
        let newOrder = Order(from: order)!
        newOrder.status = toStatus
        
        let currentUserId = HonkioApi.activeUser()?.userId
        
        let orderSetStrategy = HKOrderSetVCStrategy(tag, parent: self)
        orderSetStrategy.removeOnCompletion = true
        orderSetStrategy.isPinRequired = true
        orderSetStrategy.isAccountRequired = askAccount
        orderSetStrategy.useMerchantApi = order.userOwner != currentUserId && order.thirdPerson != currentUserId
        orderSetStrategy.userSetOrder(newOrder, flags: 0)
    }

}
