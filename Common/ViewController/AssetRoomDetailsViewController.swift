//
//  AssetRoomDetailsViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/26/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class AssetRoomDetailsViewController: BaseAssetDetailsViewController {

    override open func initDataSource() {
        super.initDataSource()
        
        let section = HKTableViewSection()
        
        section.items.append(DetailedItemCell
            .buildSourceItem(Str.screen_room_details_building_name, asset?.name ?? "-"))
        
        section.items.append(DetailedItemCell
            .buildSourceItem(Str.screen_room_details_address, asset?.address?.fullAddress() ?? "-", TagLocation))
        
        self.itemsSource.append(section)
    }
}
