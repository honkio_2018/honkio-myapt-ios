//
//  AssetDetailsViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/26/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class AssetDetailsViewController: HKBaseAssetViewController {

    override func buildViewController(_ contentItem: HKAsset) -> UIViewController {
        var controller: HKAssetViewProtocol!
        switch contentItem.stricture.objectId {
        case HKAsset.Structure.ROOM:
            controller = AppController.instance.getViewControllerByIdentifier("AssetRoomDetailsViewController", viewController: self) as? HKAssetViewProtocol
            break
        case HKAsset.Structure.SAUNA:
            controller = AppController.instance.getViewControllerByIdentifier("AssetSaunaDetailsViewController", viewController: self) as? HKAssetViewProtocol
            break
        default:
            controller = AppController.instance.getViewControllerByIdentifier("AssetSaunaDetailsViewController", viewController: self) as? HKAssetViewProtocol
            break
        }
        controller?.asset = contentItem
        return controller as! UIViewController
    }
}
