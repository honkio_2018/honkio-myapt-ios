//
//  OrderViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/22/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OrderViewController: HKBaseOrderViewController {
    
    override func buildViewController(_ contentItem: Order) -> UIViewController {
        switch contentItem.model {
        case Order.Model.BOOK:
            let orderVc = AppController.instance.getViewControllerByIdentifier("BookOrderDetailsViewController", viewController: self) as! BookOrderDetailsViewController
            orderVc.order = order
            return orderVc
        default:
            return UIViewController()
        }
    }
    
    override func loadingError(_ error: ApiError) {
        
        let fallAction = { [weak self] in
            if let controllers = self?.navigationController?.viewControllers, controllers.count > 1 {
                self?.navigationController?.popViewController(animated: true)
            }
            else {
                self?.dismiss(animated: true, completion: nil)
            }
        }
        
        if !AppErrors.handleError(self, error: error, fallAction: fallAction) {
            if error.code == ErrUserOrder.notFound.rawValue {
                CustomAlertView(okTitle: "dlg_load_order_error_title".localized,
                                okMessage: "dlg_load_order_error_message".localized,
                                okAction: fallAction).show()
            }
            else {
                CustomAlertView(apiError: error, okAction: fallAction).show()
            }
        }
    }
    
}
