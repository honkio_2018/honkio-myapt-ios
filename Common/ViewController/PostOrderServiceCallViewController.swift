//
//  PostOrderViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/8/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class PostOrderServiceCallViewController: HKBaseOrderViewController {
    
    @IBOutlet weak var buttonNext: UIButton!
//    @IBOutlet weak var textComment: CustomUITextView!
    
//    public var asset: HKAsset!
//    public var products: [BookedProduct]!
//    public var event: Event!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        buttonNext.setSolidBackground()
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(PostOrderViewController.actionNext))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.textComment.becomeFirstResponder()
    }
    
    @IBAction func actionNext(_ sender: Any) {
//        let order = Order()!
//        order.userOwner = HonkioApi.activeUser()?.userId
//        order.currency = HonkioApi.mainShopInfo()?.merchant?.currency
//        order.asset = self.asset.assetId
//        order.model = Order.Model.BOOK
//        order.status = Order.BookStatus.BOOK_REQUEST
//        order.creationDate = Date()
//        order.title = self.asset.name
//        order.startDate = self.event.start
//        order.endDate = calculateEndDate()
//        order.products = self.products
//        order.comment = self.textComment.text
        
        let controller = OrderServiceCallViewController()
        controller.order = order

        self.navigationController?.pushViewController(controller, animated: true)
    }
    
//    private func calculateEndDate() -> Date {
//        var durationMillisec = 0
//        for book in self.products {
//            durationMillisec += book.product.duration * Int(book.count)
//        }
//
//        var dateComp = DateComponents()
//        dateComp.second = durationMillisec / 1000
//
//        return Calendar.current.date(byAdding: dateComp, to: self.event.start)!
//    }

}
