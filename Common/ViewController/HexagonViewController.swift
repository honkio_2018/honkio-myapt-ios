//
//  MainViewController.swift
//  WorkPilots
//
//  Created by developer on 1/4/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class HexagonViewController: HKBaseViewController, UICollectionViewDelegate {
    
    @IBOutlet weak var layout: CCHexagonFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let reusableIdentifier = "HexagonCell"
    
    var itemsSource: GenericCollectionViewDataSource<HexagonViewCell, CollectionCellItem>?
    
    var cellScale : Float = 1.0
    
    @IBAction func actionMainMenu() {
        if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "app_name".localized(table: Target.LocalizableTable)
        initUI()
        
        self.itemsSource = GenericCollectionViewDataSource<HexagonViewCell, CollectionCellItem>(reusableIdentifier: self.reusableIdentifier, initializeCollectionViewAction: { (viewCell, item) in
            viewCell.borderColor = UIColor(netHex: AppColors.HexagonBorderColor);
            viewCell.fillColor = UIColor.clear;
            viewCell.lineWidth = 4
            viewCell.cornerRadius = 10
            viewCell.backgroundColor = item.backGroundColor
            viewCell.image.image = UIImage(named: item.imageName)
            viewCell.image.tintColor = item.imageTintColor
            viewCell.titleLabel.text = item.primaryText
            viewCell.titleLabel.textColor = item.primaryTintColor
            viewCell.initialize()
        })
        
        self.buildDataSource()
        
        self.collectionView.dataSource = self.itemsSource
        self.collectionView.delegate = self
        self.collectionView.reloadData()
    }
    
    fileprivate func initUI() {
        
        var frame = self.view.frame
        
        if #available(iOS 11.0, *) {
            // do nothing
        } else if let navigationFrame = self.navigationController?.navigationBar.frame {
            
            let y = navigationFrame.maxY
            self.collectionView.contentInset = UIEdgeInsets(top: y, left: 0, bottom: 0, right: 0)
            frame = CGRect(x: 0, y: y, width: frame.size.width, height: frame.size.height - y)
        }
        
        if let tabBarFrame = self.tabBarController?.tabBar.frame {
            
            frame = CGRect(x: 0, y: frame.origin.y, width: frame.size.width, height: frame.size.height - tabBarFrame.size.height)
        }
        
        let screenSize = UIScreen.main.bounds
        
        let itemWidth = screenSize.width * 0.49
        let minimumLineSpacing = -screenSize.width / 30.0
        let minimumInteritemSpacing = -itemWidth * 0.16 + minimumLineSpacing / 2
        let itemSize = CGSize(width: itemWidth, height: itemWidth)
        let offsetVertical: CGFloat = (screenSize.height - (itemWidth * 3.6)) / 2.0
        let offsetHorizontal = (screenSize.width - (itemWidth * 2.0 + minimumInteritemSpacing)) / 2.0
        
        self.layout.sectionInset = UIEdgeInsets(top: offsetVertical, left: offsetHorizontal, bottom: 0, right: 0)
        self.layout.minimumInteritemSpacing = minimumInteritemSpacing
        self.layout.minimumLineSpacing = minimumLineSpacing
        self.layout.gap = (itemWidth + minimumLineSpacing) / 2
        self.layout.itemSize = itemSize
        
        cellScale = Float(itemSize.width) / 160.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let section = self.itemsSource?.sections[indexPath.section] {
            
            let item = section.items[indexPath.row]
            
            switch item.cellType {
                
            case .tabSauna:
                self.tabBarController?.selectedIndex = 1
                break
                
            case .tabCharge:
                self.tabBarController?.selectedIndex = 2
                break
              
            case .tabPoll:
                self.tabBarController?.selectedIndex = 3
                break
                
            case .tabInvoice:
                self.tabBarController?.selectedIndex = 4
                break
                
            case .tabServiceCall:
                self.tabBarController?.selectedIndex = 5
                break

            default:
                break
            }
        }
    }
    
    fileprivate func buildDataSource() {
        
        let collectionSection = HKCollectionSection<CollectionCellItem>()

        collectionSection.items.append(CollectionCellItem() {
            $0.imageName = "ic_sauna";
            $0.cellType = .tabSauna
            $0.cellScale = self.cellScale
            $0.imageTintColor = UIColor.white
            $0.primaryText = Str.screen_landing_button_assets_title
            $0.primaryTintColor = UIColor(netHex: AppColors.MainTextColor)
            $0.backGroundColor = UIColor(netHex: AppColors.Main1) })
        
        collectionSection.items.append(CollectionCellItem() {
            $0.imageName = "ic_charge";
            $0.cellType = .tabCharge
            $0.cellScale = self.cellScale
            $0.imageTintColor = UIColor.white
            $0.primaryText = Str.screen_landing_button_charge_car_title
            $0.primaryTintColor = UIColor(netHex: AppColors.MainTextColor)
            $0.backGroundColor = UIColor(netHex: AppColors.Main2) })
        
        collectionSection.items.append(CollectionCellItem() {
            $0.imageName = "ic_invoice"
            $0.cellType = .tabInvoice
            $0.cellScale = self.cellScale
            $0.imageTintColor = UIColor.white
            $0.primaryText = Str.screen_landing_button_invoices_title
            $0.primaryTintColor = UIColor(netHex: AppColors.MainTextColor)
            $0.backGroundColor = UIColor(netHex: AppColors.Main2) })
        
        collectionSection.items.append(CollectionCellItem() {
            $0.imageName = "ic_poll"
            $0.cellType = .tabPoll
            $0.cellScale = self.cellScale
            $0.imageTintColor = UIColor.white
            $0.primaryText = Str.screen_landing_button_polls_title
            $0.primaryTintColor = UIColor(netHex: AppColors.MainTextColor)
            $0.backGroundColor = UIColor(netHex: AppColors.Main1) })
        
        collectionSection.items.append(CollectionCellItem() {
            $0.imageName = "ic_service_call"
            $0.cellType = .tabServiceCall
            $0.cellScale = self.cellScale
            $0.imageTintColor = UIColor.white
            $0.primaryText = Str.screen_create_order_service_call_title
            $0.primaryTintColor = UIColor(netHex: AppColors.MainTextColor)
            $0.backGroundColor = UIColor(netHex: AppColors.Main1) })
        
        self.itemsSource?.sections.append(collectionSection)
    }
}
