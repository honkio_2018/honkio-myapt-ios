//
//  HorsesMapViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/29/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import MapKit
import HonkioAppTemplate

class AssetsMapViewController: HKBaseAssetsMapViewController {
    
    @IBOutlet weak var appleMapsView: MKMapView!
    @IBOutlet weak var btnFilter: UIButton!
    
    private var bottomSheetViewController: BottomSheetViewController?
    
    @IBAction func actionMainMenu() {
        if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func filterAction(_ sender: Any?) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: Str.screen_asset_map_filter_category_all, style: .default) {
            [unowned self] action in
            self.filter.structureIds = HKAsset.AllStructures
            self.updateFilterUI()
            self.refresh()
        })
        
        for structureId in HKAsset.AllStructures {
            if let structure = AppApi.instance.assetStructures[structureId] {
                alertController.addAction(UIAlertAction(title: structure.name, style: .default) {
                    [unowned self] action in
                    self.filter.structureIds = [structure.objectId]
                    self.updateFilterUI()
                    self.refresh()
                })
            }
        }
        
        self.present(alertController, animated: true)
    }
    
    override func viewDidLoad() {
        self.title = "app_name".localized(table: Target.LocalizableTable)
        self.filter.structureIds = HKAsset.AllStructures
        super.viewDidLoad()
        self.updateFilterUI()
    }
    
    override func buildMapViewProtocol() -> HKMapViewProtocol {
        let mapView = HKAppleMapsView(self.appleMapsView)
        mapView.showsUserLocation = true
        if let location = HonkioApi.deviceLocation() {
            mapView.moveToLocation(location.coordinate, animated: false)
        }
        return mapView
    }
    
    @IBAction func currentLocationAction(_ sender: AnyObject) {
        moveToUserLocation(animated: true)
    }
    
    override open func annotationDidSelect(_ annotation: HKMapViewAnnotation?) {
        if annotation != nil {
            if let asset = annotation?.item as? HKAsset {
                addBottomSheetView(asset)
            }
        }
        else {
            if let bottomSheetVC = self.bottomSheetViewController, !self.children.isEmpty {
                bottomSheetVC.willMove(toParent: nil)
                bottomSheetVC.view.removeFromSuperview()
                bottomSheetVC.removeFromParent()
                bottomSheetViewController = nil
            }
        }
    }

    func addBottomSheetView(_ asset: HKAsset) {
        bottomSheetViewController = BottomSheetViewController()
        if let bottomSheetVC = bottomSheetViewController {
            bottomSheetVC.assets = asset
            
            self.addChild(bottomSheetVC)
            self.view.addSubview(bottomSheetVC.view)
            bottomSheetVC.didMove(toParent: self)
            
            let height = view.frame.height
            let width  = view.frame.width
            bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
        }
    }
    
    private func updateFilterUI() {
        if self.filter.structureIds?.count ?? 0 == 1, let structure = AppApi.instance.assetStructures[self.filter.structureIds![0]] {
            self.btnFilter.setTitle(structure.name, for: .normal)
        }
        else {
            self.btnFilter.setTitle(Str.screen_asset_map_filter_category_all, for: .normal)
        }
    }
}
