//
//  OrdersListViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/25/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OrdersServiceCallListViewController: HKBaseOrdersListViewController {
    
    @IBOutlet weak var btnFilter: UIButton!
    
    private var serviceCallFilter: String?
    
    let dateFormatter = DateFormatter()
    
    @IBAction func filterAction(_ sender: Any?) {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
            
            alertController.addAction(UIAlertAction(title: "screen_service_call_list_filter_new".localized, style: .default) {
                [unowned self] action in
                self.serviceCallFilter = nil
                self.updateFilterUI()
                self.refresh()
            })
            alertController.addAction(UIAlertAction(title: "screen_service_call_list_filter_history".localized, style: .default) {
                [unowned self] action in
                self.serviceCallFilter = "history"
                self.updateFilterUI()
                self.refresh()
            })
            self.present(alertController, animated: true)
//        }
    }
    
    open override func viewDidLoad() {
  
        super.viewDidLoad()
        self.title = "app_name".localized(table: Target.LocalizableTable)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 64
        tableView.tableFooterView = UIView()
        
        dateFormatter.setLocalizedDateFormatFromTemplate("d/M-y")
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.btnFilter.setTitle("screen_service_call_list_filter_new".localized, for: .normal)
        super.viewWillAppear(true)
    }
    
    @IBAction func actionMainMenu() {
        if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func actionAddOrderServiceCall(_ sender: Any?) {
            if let viewController = AppController.instance.getViewControllerByIdentifier("AddOrderServiceCallViewController", viewController: self) as? AddOrderServiceCallViewController {

                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        
    }
    //MARK: - HONKIO
    
    override open func setupFilter(_ filter: OrderFilter, count: Int, skip: Int) {
        super.setupFilter(filter, count: count, skip: skip)
        filter.queryProductDetails = true
        filter.model = Order.Model.SERVICE_CALL
        filter.owner = HonkioApi.activeUser()?.userId

//        Order.ServiceCallStatus.REQUEST
//
        if self.serviceCallFilter != "history" {
            filter.statuses = ["new","in_progress"]
        }
        else {
            filter.statuses = ["cancelled","completed"]
        }
    }

    private func updateFilterUI() {
        switch self.serviceCallFilter {
        
        case "history" :
            self.btnFilter.setTitle("screen_service_call_list_filter_history".localized, for: .normal)
            break
            
        default:
            self.btnFilter.setTitle("screen_service_call_list_filter_new".localized, for: .normal)
            break
        }
    }

    
    override open func cellIdentifier(_ item: Order) -> String {
        return "OrderServiceCallTableViewCell"
    }
    
    override open func buildCellBunder() -> HKTableViewItemInitializer {
        return { (viewController, cell, item) in
            (viewController as? OrdersServiceCallListViewController)?.bindCell(cell as! OrderServiceCallTableViewCell, order: item as! Order)
        }
    }
    
    open func bindCell(_ orderCell: OrderServiceCallTableViewCell, order: Order) {
        orderCell.dateLabel.text = dateFormatter.string(from: order.creationDate)
        orderCell.titleLabel.text = order.title
        orderCell.statusLabel.text = order.status
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        if let order = self.lazyItem(indexPath) as? Order {
            order.unreadChatMessagesCount = 0
            tableView.reloadData()
            
            let controller = OrderServiceCallViewController()
            controller.order = order
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
