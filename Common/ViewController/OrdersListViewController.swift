//
//  OrdersListViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/25/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OrdersListViewController: HKBaseOrdersListViewController {

    let dateFormatter = DateFormatter()
    private var ordersFilter: String?
    
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBAction func actionMainMenu() {
        if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func filterAction(_ sender: Any?) {
      
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: "screen_orders_list_filter_new".localized, style: .default) {
            [unowned self] action in
            self.ordersFilter = nil
            self.updateFilterUI()
            self.refresh()
        })
        alertController.addAction(UIAlertAction(title: "screen_orders_list_filter_history".localized, style: .default) {
            [unowned self] action in
            self.ordersFilter = "history"
            self.updateFilterUI()
            self.refresh()
        })
        self.present(alertController, animated: true)
        //        }
    }

    
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "app_name".localized(table: Target.LocalizableTable)
        self.btnFilter.setTitle("screen_orders_list_filter_new".localized, for: .normal)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 64
        tableView.tableFooterView = UIView()
        
        dateFormatter.setLocalizedDateFormatFromTemplate("d/M-y")
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
    }
    
    //MARK: - HONKIO
    override open func setupFilter(_ filter: OrderFilter, count: Int, skip: Int) {
        super.setupFilter(filter, count: count, skip: skip)
        filter.queryProductDetails = true
        filter.model = Order.Model.BOOK
        filter.owner = HonkioApi.activeUser()?.userId
        
        //        Order.Status.REQUEST
        //
        if self.ordersFilter != "history" {
            filter.statuses = ["book_request","booked"]
        }
        else {
            filter.statuses = ["cancelled_by_user","cancelled_by_merchant","completed"]
        }
    }
    
    private func updateFilterUI() {
        switch self.ordersFilter {
            
        case "history" :
            self.btnFilter.setTitle("screen_orders_list_filter_history".localized, for: .normal)
            break
            
        default:
            self.btnFilter.setTitle("screen_orders_list_filter_new".localized, for: .normal)
            break
        }
    }
    
    override open func cellIdentifier(_ item: Order) -> String {
        return "OrderTableViewCell"
    }
    
    override open func buildCellBunder() -> HKTableViewItemInitializer {
        return { (viewController, cell, item) in
            (viewController as? OrdersListViewController)?.bindCell(cell as! OrderTableViewCell, order: item as! Order)
        }
    }
    
    open func bindCell(_ orderCell: OrderTableViewCell, order: Order) {
        orderCell.dateLabel.text = dateFormatter.string(from: order.creationDate)
        orderCell.titleLabel.text = order.title
        orderCell.descLabel.text = order.statusName
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        if let order = self.lazyItem(indexPath) as? Order {
            order.unreadChatMessagesCount = 0
            tableView.reloadData()
            
            let controller = OrderViewController()
            controller.order = order
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
