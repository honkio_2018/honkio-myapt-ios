//
//  MediaPagesViewController.swift
//  honkio-indx-ios
//
//  Created by Shurygin Denis on 12/6/18.
//  Copyright © 2018 RiskPointer. All rights reserved.
//

import UIKit
import AVKit
import HonkioAppTemplate

class MediaPagesViewController: HKBaseMediaPagesViewController {
    
    private var interactivePopGestureRecognizerIsEnabled: Bool? = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePopGestureRecognizerIsEnabled = self.navigationController?.interactivePopGestureRecognizer?.isEnabled
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.interactivePopGestureRecognizerIsEnabled ?? true
    }
    
    override func viewControllerFor(_ mediaFile: HKMediaFile) -> UIViewController {
        if mediaFile.type == HK_MEDIA_FILE_TYPE_IMAGE {
            let viewController = AppController.instance.getViewControllerByIdentifier("PhotoViewController", viewController: self) as! PhotoViewController
            viewController.loadFromUrl(mediaFile.url)
            return viewController
        }
        else if mediaFile.type == HK_MEDIA_FILE_TYPE_VIDEO {
            if let url = mediaFile.url {
                let viewController = AVPlayerViewController()
                viewController.player = AVPlayer(url: URL(string: url)!)
                return viewController
            }
        }
        return UIViewController()
    }
}
