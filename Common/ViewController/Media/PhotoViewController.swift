//
//  PhotoViewController.swift
//  honkio-indx-ios
//
//  Created by Shurygin Denis on 12/6/18.
//  Copyright © 2018 RiskPointer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class PhotoViewController: HKBaseViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    public func loadFromUrl(_ url: String) {
        self.imageView.imageFromURL(link: url, contentMode: UIView.ContentMode.scaleAspectFit, fallAction: {})
    }
}
