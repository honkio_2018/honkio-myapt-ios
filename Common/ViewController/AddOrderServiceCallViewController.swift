//
//  EditAssetProductViewController.swift
//  honkio-indx-ios
//
//  Created by Shurygin Denis on 12/5/18.
//  Copyright © 2018 RiskPointer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class AddOrderServiceCallViewController: HKWizardViewController {
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var locationDescriptionTextView: UITextView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var DescriptionTextView: UITextView!

    private var isUrgent: Bool = false
    
    private var locationLat: Double?
    private var locationLon: Double?
    private var address: HKAddress?

    @IBAction func urgentSwitchAction(_ sender: UISwitch) {
        self.isUrgent = sender.isOn ? true : false
    }
    
    @IBAction func editeLocationAction(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        locationDescriptionTextView!.layer.borderWidth = 2
        locationDescriptionTextView!.layer.cornerRadius = 4
        locationDescriptionTextView!.clipsToBounds = true
        locationDescriptionTextView!.layer.borderColor = UIColor.init(hex: 16382457, alpha: 1).cgColor

        DescriptionTextView!.layer.borderWidth = 2
        DescriptionTextView!.layer.cornerRadius = 4
        DescriptionTextView!.clipsToBounds = true
        DescriptionTextView!.layer.borderColor = UIColor.init(hex: 16382457, alpha: 1).cgColor
        self.hideKeyboardWhenTappedAround()
        self.nameLabel.becomeFirstResponder()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(AddOrderServiceCallViewController.actionNext))

    }
    @IBAction func actionSetLocation() {
        if let viewController = AppController.instance.getViewControllerByIdentifier("MapViewController", viewController: self) as? MapViewController {
            
            viewController.title = "screen_select_on_map_title".localized
            viewController.readOnly = false
            
            if let lat = self.locationLat, let lon = self.locationLon {
                viewController.location = CLLocation(latitude: lat, longitude: lon)
            }
            
            viewController.completeAction = { [weak self] (viewController: MapViewController, place: HKAddress?, location: CLLocation?) in
                self?.locationLat = location?.coordinate.latitude
                self?.locationLon = location?.coordinate.longitude
                self?.address = place
                self?.updateLocationView()
                self?.navigationController?.popViewController(animated: true)
            }
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    open func updateUI() {
        self.updateLocationView()
    }
    
    private func updateLocationView() {
        if !self.isLocationSet() {
            self.locationLabel.text = "screen_edit_asset_location_not_set".localized
        }
        else {
            self.locationLabel.text = self.address?.fullAddress() ?? String(format: "%.5f, %.5f", self.locationLat ?? 0.0, self.locationLon ?? 0.0)
        }
    }
    
    private func isLocationSet() -> Bool {
        return self.locationLat != nil && self.locationLon != nil && !(self.locationLat == 0 && self.locationLon == 0)
    }
    
    @IBAction func actionNext(_ sender: Any?) {
        let order = Order()!
        order.userOwner = HonkioApi.activeUser()?.userId
        order.model = Order.Model.SERVICE_CALL
        order.status = Order.ServiceCallStatus.REQUEST
        order.creationDate = Date()
        order.title = self.nameLabel.text
        order.desc = self.DescriptionTextView.text
        order.isUrgent = self.isUrgent
        order.locationDescription = self.locationDescriptionTextView.text

        if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: AddAddressOrderServiceCallViewController.self), viewController: self) as? AddAddressOrderServiceCallViewController
        {
            controller.order = order
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
