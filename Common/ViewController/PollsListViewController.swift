//
//  PollsListViewController.swift
//  myappartment-ios
//
//  Created by Shurygin Denis on 3/21/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class PollsListViewController: HKBasePollsListViewController {
    
    @IBAction func actionMainMenu() {
        if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "app_name".localized(table: Target.LocalizableTable)
        self.filter.queryActive = true
        self.filter.queryVisible = true
        self.filter.queryResults = true
        self.filter.queryVotes = true
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let optionItem = self.lazyItem(indexPath) as? (poll: HKPoll, option: String) {
            if optionItem.poll.userAnswer == nil {
                CustomInputAlertView(title: Str.screen_polls_list_vote_dialog_title, message: Str.screen_polls_list_vote_dialog_comment_title) {[unowned self] (text) in
                    
                    BusyIndicator.shared.showProgressView(self.view)
                    self.userPollVote(self.shopIdentity, optionItem: optionItem, comment: text, handler: {[weak self] (response) in
                        BusyIndicator.shared.hideProgressView()
                        if let strongSelf = self {
                            if response.isStatusAccept() {
                                CustomAlertView(okTitle: nil, okMessage: Str.screen_polls_list_vote_dialog_vote_send, okAction: nil).show()
                            }
                            else {
                                let error = response.anyError()
                                if !AppErrors.handleError(strongSelf, error: error) {
                                    CustomAlertView(apiError: error, okAction: nil).show()
                                }
                            }
                        }
                    })
                    
                }.hint(Str.screen_polls_list_vote_dialog_comment_hint).show()
            }
        }
    }
    
    override open func cellIdentifier(_ item: HKPoll) -> String {
        return "PollItemCell"
    }
    
    override open func cellIdentifier(_ item: HKPoll, forOption option: String) -> String {
        if item.userAnswer == nil {
            return "PollOptionItemCell"
        }
        else {
            return "PollOptionResultItemCell"
        }
    }
    
    override open func buildCellBunder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let poolCell = cell as! PollItemCell
            let poll = item as! HKPoll
            
            poolCell.labelTitle.text = poll.title
            poolCell.labelText.text = poll.text
        }
    }
    
    override open func buildOptionCellBunder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let cortage = item as! (poll: HKPoll, option: String)
            let poll = cortage.poll
            let option = poll.options[cortage.option] as! HKString
            
            if poll.userAnswer != nil {
                let resultCell = cell as! PollOptionResultItemCell
                
                resultCell.labelTitle.text = option.toString()
                resultCell.labelValue.text = String(format: "%d", poll.answers?[cortage.option]?.intValue ?? 0)
                resultCell.imgCheck.isHidden = poll.userAnswer != cortage.option
            }
            else {
                let optionCell = cell as! PollOptionItemCell
                
                optionCell.labelTitle.text = option.toString()
            }
        }
    }
}
