//
//  AppViewControllerFactory.swift
//  honkio-safetypoint-ios
//
//  Created by Shurygin Denis on 12/4/18.
//  Copyright © 2018 RiskPointer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class AppViewControllerFactory: HKBaseAppViewControllerFactory {
    
    override func getStoryboardIdentifierByViewControllerId(_ identifier: Int) -> String? {
        switch identifier {
//        case AppController.Login:
//            return "AppLoginViewController"
        case AppController.MediaFiles:
            return "MediaPagesViewController"
        default:
            return super.getStoryboardIdentifierByViewControllerId(identifier)
        }
    }
    
//    override func getViewControllerById(_ controllerId: Int, viewController: UIViewController) -> UIViewController? {
//        switch controllerId {
//        case AppController.Asset:
//            return AssetViewController()
//        default:
//            return super.getViewControllerById(controllerId, viewController: viewController)
//        }
//    }
    
    override func isGuestModeAvailable() -> Bool {
        return true
    }
    
}
